#LIS4381_P1
##Mobile Development
###Nhi Tran

### Lessons Learned###

* Switching activities using intent
* onClick buttons
* Changing appearance of app: background, icons, shadow
* Customizing Action Bar with Launcher icon

![Screen Shot 2016-06-26 at 11.24.56 PM.png](https://bitbucket.org/repo/yabE9E/images/2747509956-Screen%20Shot%202016-06-26%20at%2011.24.56%20PM.png)

![Screen Shot 2016-06-26 at 11.25.02 PM.png](https://bitbucket.org/repo/yabE9E/images/3773020490-Screen%20Shot%202016-06-26%20at%2011.25.02%20PM.png)

![Screen Shot 2016-06-26 at 11.25.07 PM.png](https://bitbucket.org/repo/yabE9E/images/1020979626-Screen%20Shot%202016-06-26%20at%2011.25.07%20PM.png)