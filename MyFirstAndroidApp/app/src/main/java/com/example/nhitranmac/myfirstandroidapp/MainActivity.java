package com.example.nhitranmac.myfirstandroidapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
// username
    // password
    // login
    public void clickFunction( View view )
    {
        //Log.i("Info", "Button Tapped!");
        // create a variable for that text field
        // get the value for that variable
        //findViewById is impatible to EditText (EditText) will convert View to EditText
        EditText myTextField = (EditText)findViewById(R.id.textField);
        // toString will convert to string and populate the logs
        Log.i("TextFieldValue", myTextField.getText().toString());

    }
    public void logIn( View view )
    {
        EditText usernameField = (EditText)findViewById(R.id.usernameText);
        EditText passwrdField = (EditText)findViewById(R.id.passwordText);
        Log.i("UsernameFieldValue", usernameField.getText().toString());
        Log.i("PasswordFieldVale", passwrdField.getText().toString());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
