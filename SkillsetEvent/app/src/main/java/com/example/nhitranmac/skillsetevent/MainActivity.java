package com.example.nhitranmac.skillsetevent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.Locale;

import android.util.Log;
import android.widget.Button;
import android.widget.Spinner;

import android.widget.EditText;
import android.widget.TextView;

import android.view.View;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner1;
    private Button btnSubmit;
    double costPerTicket = 0.0;
    int numberOfTickets = 0;
    double totalCost = 0.0;
    String groupChoice = "";

/*
 <string name="app_name">Skillset Event</string>
    <string name="txtTitle">Ticket Value</string>
    <string name="txtTickets">Number of Tickets</string>
    <string name="prompt">Select-group</string>
    <string name="description">Concert Image</string>
    <string name="btnCost">Calculate</string>
    <string-array name="txtGroup">
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final EditText tickets = (EditText)findViewById(R.id.editText);
        final Spinner group = (Spinner)findViewById(R.id.spinner1);
        Button cost = (Button)findViewById(R.id.button);

        cost.setOnClickListener(new View.OnClickListener(){
            final TextView result = ((TextView) findViewById(R.id.calculatedText));
            public void onClick(View v)
            {
                groupChoice = group.getSelectedItem().toString();
                if(groupChoice.equals("Van Halen"))
                {
                    costPerTicket=59.99;
                }
                else if(groupChoice.equals("Led Zeppelin"))
                {
                    costPerTicket=49.99;
                }
                else
                {
                    costPerTicket=39.99;
                }
                numberOfTickets = Integer.parseInt(tickets.getText().toString());
                totalCost = costPerTicket * numberOfTickets;
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                result.setText(String.format("Cost for " + groupChoice + " is" + nf.format(totalCost)));
            }
        });

        addListenerOnButton();
        addListenerOnSpinnerItemSelection();

    }

    public void addListenerOnSpinnerItemSelection() {
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    // get the selected dropdown list value
    public void addListenerOnButton() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        btnSubmit = (Button) findViewById(R.id.button);

//        btnSubmit.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(MainActivity.this,
//                        "OnClickListener : " +
//                                "\nSpinner 1 : " + String.valueOf(spinner1.getSelectedItem()),
//                        Toast.LENGTH_SHORT).show();
//            }
//        }
    }

}
