package com.example.nhitranmac.toastname;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;


public class MainActivity extends AppCompatActivity {

    public void clickFunction(View view) {
        EditText nameField = (EditText)findViewById(R.id.nameText);
        String temp = "Hello " + nameField.getText().toString();
        Toast.makeText(getApplicationContext(), temp, Toast.LENGTH_LONG).show();
        Log.i("TextFieldValue", temp);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
